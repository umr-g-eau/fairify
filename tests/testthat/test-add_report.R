test_that("add_report should raise message if reports does not exists", {
  path <- tempfile(pattern = "dir")
  dir.create(path, recursive = TRUE)
  expect_message(add_report("test", path = path))
  unlink(path, recursive = TRUE)
})

test_that("add_report should raise error if template does not exists", {
  path <- tempfile(pattern = "dir")
  dir.create(path, recursive = TRUE)
  expect_error(add_report("test", template = "wrong_syntax", path = path),
               regexp = "format")
  expect_error(add_report("test", template = "ggplot2:inrae", path = path),
               regexp = "templates")
  expect_error(add_report("test", template = "fairify:wrong_template", path = path),
               regexp = "template")
  unlink(path, recursive = TRUE)
})

test_that("add_report should create a report", {
  path <- tempfile(pattern = "dir")
  dir.create(path, recursive = TRUE)
  add_report("test", template = "fairify:umr_geau", path = path)
  sapply(file.path(path,
                   "reports/test",
                   c(list.files(pkg_sys("bookdown_template")),
                     "_fairify.yml",
                     "_output.yml")),
         function(x) expect_true(file.exists(!!x)))
  expect_equal(readLines(file.path(path, "reports/test/_fairify.yml")),
               "template: fairify:umr_geau")
  output_yml <- yaml::read_yaml(file.path(path, "reports/test/_output.yml"))
  expect_true(grepl("templates/inrae", output_yml$`bookdown::gitbook`$config$toc$before))
  unlink(path, recursive = TRUE)
})
