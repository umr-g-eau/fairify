# Create a temporary fairify project
path <- tempfile(pattern = "loadConfig")
create_fairify(path, open = FALSE, git = FALSE)

# Load the created fairify project
pkgload::load_all(path)

# Load the default config provided by fairify
cfg <- loadConfig()
str(cfg)
