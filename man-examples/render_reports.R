# Create a fairify project in a temporary folder
path <- tempfile(pattern = "fairify_")
fairify::create_fairify(path, git = FALSE, open = FALSE)
owd <- setwd(path)

# Create a report
fairify::add_report("my_report")

# Add a chapter
writeLines(
  c(
    "# A new chapter",
    "",
    "With a new paragraph."
  ),
  file.path(path, "reports", "my_report", "01-new_chapter.Rmd")
)

# Render the report in HTML format
render_reports(reports_dir = file.path(path, "reports"))

# Render the report in PDF format
render_reports(reports_dir = file.path(path, "reports"),
               output_format = "bookdown::pdf_book")

# Back to initial working directory
setwd(owd)
