---
title: "Titre du rapport"
author: "Auteur du rapport"
date: "Date du rapport `r Sys.Date()`"
site: bookdown::bookdown_site
documentclass: report
bibliography:
  - references.bib
biblio-style: authoryear
link-citations: yes
description: ""
always_allow_html: true
---

# Introduction to fairify report

## Global options: the "setup.R" script

Before each Rmd document composing the report the script "setup.R" located in the
same folder is executed. It prepares the report for knitting and loads global
configuration given by the templates. It can also be used by you to add your own
settings like [chunk options](https://yihui.org/knitr/options/).

## How to cite and handle a bibliography

See the [Citations section in the bookdown reference book](https://bookdown.org/yihui/bookdown/citations.html).


An example of a citation in the text without brackets: @knuth84.

An example in brackets [@knuth84].

## Equations

Excerpt from [Section 2.2.1 of bookdown reference book](https://bookdown.org/yihui/bookdown/markdown-extensions-by-bookdown.html#equations).

To number and refer to equations\index{equation}\index{cross-reference}, put them in the equation environments and assign labels to them using the syntax `(\#eq:label)`, e.g.,

```latex
\begin{equation}
  f\left(k\right) = \binom{n}{k} p^k\left(1-p\right)^{n-k}
  (\#eq:binom)
\end{equation}
```

It renders the equation below:

\begin{equation}
f\left(k\right)=\binom{n}{k}p^k\left(1-p\right)^{n-k} (\#eq:binom)
\end{equation}

You may refer to it using `\@ref(eq:binom)`, e.g., see Equation \@ref(eq:binom).

